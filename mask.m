%--------------------------------------------------------------------------
%Organisation: School of Engineering, University of Edinburgh
%Project: BEng Project
%Name: Xu Zuo s1678439
%Description: This function is designed to divide image pixels into two
%sets. The free pixels consists of boundary pixels with some nonboundary
%pixels selected in randomized procedure. Let 0<p<=1 be the fix
%probability. Each element of the nonboundary pixels is included in free
%pixel set with 1-p independently. The rest of image pixels are included in
%fixed pixel set.
%--------------------------------------------------------------------------

function x_mask = mask(x,fix_p)
%Input:  x               Segmented image,N by N array
%        fix_p           Fix probability, between 0 and 1
%Output: x_mask          The set of free pixels
		
            random = 1 - fix_p;
			r = 1; %Radius of masking kernel.
			w = 2 * r + 1;
			
            %Use 8-neighbourhood connectivity
            %Create an 3x3 array, with kernel(2,2) being the central pixel,
            %and the remaining 8 pixels being the neighbourhood
			kernel = ones(2*r+1, 2*r+1);
			
			%Create window, for a NxN image, the window size is (N+2)x(N+2)
            %Add the image to the window.
			Xlarge = zeros(size(x,1)+w-1, size(x,2)+w-1); 
			Xlarge(1+r:end-r, 1+r:end-r) = x;

            %Define edges.
            %s and t indicates the direction to the central pixel.
            %eg. a central pixel can be expressed as Xlarge(a,a), then
            %Xlarge(s+a,t+a) is the 'north' pixel (with s = -1, t = 0).
            %Similarly, Xlarge(s+a,t+a) is the 'northwest' pixel (with
            %s = -1, t= -1)
			Edges = zeros(size(x));
			for s = -r:r
				for t = -r:r
					if kernel(s+r+1, t+r+1) == 0 %make sure movement is within the kernel
						continue
                    end
                    %eg. when s = -1, t = -1. Compare all pixels within the
                    %grid to the pixels located to their 'northwest'
					Temp = abs(Xlarge(1+r:end-r, 1+r:end-r) - Xlarge(1+r+s:end-r+s, 1+r+t:end-r+t));
                    %if the central pixel has diiferent values from its
                    %neighbours, the pixel is marked as a boundary pixel
					Edges(Temp > 0.5) = Edges(Temp > 0.5) + 1;  
				end
            end

            %one pixel can be defined as boundary for several times
            %remove the repeated definitions
			Edges = Edges > 1;
			
			%Select nonboundary pixels randomly, the probability that a
			%nonboundary pixel is selected is random = 1 - fix_p
			RandomField = double(rand(size(x)) < random);

            %x_mask is the free pixel set, which needs to be updated later
            %using SART within the DART loop.
            %x_mask = boundary pixels + randomly selected nonboundary
            %pixels
			x_mask = or(Edges, RandomField);
			
end