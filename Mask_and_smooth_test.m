%Generate an image,Grid Scale 100*100
%with p = 200 parallel rays for each angle
%Angle increment 180/15 = 12 different angles
N = 100; theta = 0:15:179; p = 2*N;

%Generate 3 cases
[A,b,x1] = paralleltomo1(N,theta,p);
[~,~,x2] = paralleltomo2(N,theta,p);
[~,~,x3] = paralleltomo3(N,theta,p);

%Reshape image to an N by N array
x1_reshape = reshape(x1,N,N);
x2_reshape = reshape(x2,N,N);
x3_reshape = reshape(x3,N,N);

%Detect only edges for 3 cases. Set the fix probability to 0.
fix_p = 1;
x1_mask = mask(x1_reshape,fix_p);
x2_mask = mask(x2_reshape,fix_p);
x3_mask = mask(x3_reshape,fix_p);

figure; 
subplot(1,3,1); imagesc(x1_mask); daspect([1 1 1]);
subplot(1,3,2); imagesc(x2_mask); daspect([1 1 1]);
subplot(1,3,3); imagesc(x3_mask); daspect([1 1 1]);

%Apply a Guasssian filter to images, with intensity of smoothing 0.1.
s = 0.5;
x1_soomth = smooth(x1_reshape,s);
x2_soomth = smooth(x2_reshape,s);
x3_soomth = smooth(x3_reshape,s);

figure;
subplot(1,3,1); imagesc(x1_soomth); daspect([1 1 1]);
subplot(1,3,2); imagesc(x1_soomth); daspect([1 1 1]);
subplot(1,3,3); imagesc(x1_soomth); daspect([1 1 1]);