 %histo-tomography demo

%Set dimension of NxN image/target
N=100;

%Set projection angles
thetas=linspace(0,179/180 * pi, 50);
%thetas = linspace(0,pi,7); thetas=thetas(1:6);

%Develop a 2D NxN grid in normalised pixel units [-1,1]x[-1,1]
L=N+1;
xs=linspace(-1,1,L); %in pixel units
ys=linspace(-1,1,L);
[X,Y] = meshgrid(xs,ys);
simp = [];
for i=1:L-1
    simp1 = L*(i-1)+1:i*L-1; 
    simp2 = i*L+1:(i+1)*L-1;
    simp3 = simp1+1;
    simp4 = simp2+1;
    simp=[simp; [simp1.',simp2.',simp4.',simp3.']];
end
clear simp1 simp2 simp3 simp4
vtx = [X(:),Y(:)];

%Export grid coordinates and element connectivity
gridD.vtx = vtx;
gridD.simp = simp;
nel = length(gridD.simp);

%Find centres of pixels
gridD.vtxc = zeros(nel,2);
for i=1:size(gridD.simp,1)
 gridD.vtxc(i,:) = mean(gridD.vtx(gridD.simp(i,:),:));
end


%Define a 2D phantom 
%(a) region-wise continuous, e.g. sum of two Gaussians 
sigma = [0.0325; 0.06]; mag = [2; 1.3];

%centres of the Gaussians in [-1,1]x[-1,1]
xb = [-0.4,0]; yb = [-0.4;0.6];

x = zeros(nel,1);
%find the elements hosting the centers of the gaussians
for i=1:length(xb)
    
    gmag = ((gridD.vtxc(:,1)-kron(xb(i),ones(length(gridD.vtxc),1))).^2 + ...
        (gridD.vtxc(:,2)-kron(yb(i),ones(length(gridD.vtxc),1))).^2)./(2*sigma(i));
    
    x = x + mag(i)*exp(-gmag); 
end

%Spectral resolution in bins
k=10;
bins = linspace(0,max(x)+0.1,k+1);

%Split the image into binary spectral images
[~,~,imask] = histcounts(x,bins);

%The vector imask contains the characteristic functions of Radon transform
%integrals for each bin, i.e. at the ith bin we measure \int x(imask(i)) ds

Xs=zeros(length(x),k); 
for i=1:k
Xs(imask==i,i) = x(imask==i); %(bins(i)+bins(i+1))/2;
end

%This yields sum(Xs,2)-x=0


%% Compute Radon and histo-Radon data

%In image form 
xim = reshape(x,N,N);

%Compute projection data
b = myRadon(xim, thetas, '4pt');

%and histo-Radon
bhist = zeros(size(b,1),size(b,2),k);
for i=1:k
    bhist(:,:,i)= myRadon(reshape(Xs(:,i),N,N), thetas, '4pt');
end

%This should satisfy sum(bb(a,b,:))=b(a,b);


%% Model the data algebraically using AIR
[A,~,~] = paralleltomo(N,thetas);


%Compute FBP images, for the conventional data
filter = 'ram-lak'; d = 0.9;
[im_b,Hb] = myIRadon(b, filter, thetas, d, N);

im_bb = zeros(size(im_b,1),size(im_b,2),k);
%and the histo-data
for i=1:k
    [im_bb(:,:,i),Hbb] = myIRadon(squeeze(bhist(:,:,i)), filter, thetas, d, N);
end

%Compute Lambda tomography images
lam_im_b = myLambdaIRadon(b, thetas, N, 1);

lam_im_bb = zeros(size(im_bb));

for i=1:k
   Lambdaf = myLambdaIRadon(bhist(:,:,i), thetas, N, 1);
   InvLambdaf = myLambdaIRadon(bhist(:,:,i), thetas, N, 0);
   lam_im_bb(:,:,i) = Lambdaf + InvLambdaf;
end

%Clustering
D = [gridD.vtxc(:,1),gridD.vtxc(:,2),x];
idx = kmeans(D(:,3),5);
figure; hold on;
plot3(D(idx==1,1),D(idx==1,2),x(idx==1),'.');
plot3(D(idx==2,1),D(idx==2,2),x(idx==2),'r.');
plot3(D(idx==3,1),D(idx==3,2),x(idx==3),'g.');
plot3(D(idx==4,1),D(idx==4,2),x(idx==4),'m.');
plot3(D(idx==5,1),D(idx==5,2),x(idx==5),'k.');

idx = kmeans(D,5);
figure; hold on;
plot3(D(idx==1,1),D(idx==1,2),x(idx==1),'.');
plot3(D(idx==2,1),D(idx==2,2),x(idx==2),'r.');
plot3(D(idx==3,1),D(idx==3,2),x(idx==3),'g.');
plot3(D(idx==4,1),D(idx==4,2),x(idx==4),'m.');
plot3(D(idx==5,1),D(idx==5,2),x(idx==5),'k.');

idx = kmeans(D,8);
figure; hold on;
plot3(D(idx==1,1),D(idx==1,2),x(idx==1),'.');
plot3(D(idx==2,1),D(idx==2,2),x(idx==2),'r.');
plot3(D(idx==3,1),D(idx==3,2),x(idx==3),'g.');
plot3(D(idx==4,1),D(idx==4,2),x(idx==4),'m.');
plot3(D(idx==5,1),D(idx==5,2),x(idx==5),'k.');
plot3(D(idx==6,1),D(idx==6,2),x(idx==6),'y.');
plot3(D(idx==7,1),D(idx==7,2),x(idx==7),'w.');
plot3(D(idx==8,1),D(idx==8,2),x(idx==8),'c.');
