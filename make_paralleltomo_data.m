%--------------------------------------------------------------------------
%Organisation: School of Engineering, University of Edinburgh
%Project: BEng Project
%Name: Xu Zuo s1678439
%Description: This code is used to set up 3 kinds of tomographic problems.
%The first case displays noiseless image with 5 grey levels; 
%The second case displays an iamge, whose grey levels can be described 
%as normal distributions with variance of 0.01;
%The third case displays an image whose grey levels has both uncertainty 
%and unknown components (a grey level with mean value 6 and variance 0.01).
%--------------------------------------------------------------------------

%Generate an image,Grid Scale 16*16,
%with p = 32 parallel rays for each angle
%Angle increment 180/15 = 12 different angles
N = 100; theta = 0:15:179; p = 2*N;

%The first case
[A,~,x1] = paralleltomo1(N,theta,p);

%The second case
%Add uncertainty to the image
[~,~,x2] = paralleltomo2(N,theta,p);

%The third case
[~,~,x3] = paralleltomo3(N,theta,p);

figure; 
subplot(1,3,1); imagesc(reshape(x1,N,N)); daspect([1 1 1]);
%plot(x1,'.'); axis([0 300 0 ceil(max(x3))]); 
subplot(1,3,2); imagesc(reshape(x2,N,N)); daspect([1 1 1]);
%plot(x2,'g.'); axis([0 300 0 ceil(max(x3))]); 
subplot(1,3,3); imagesc(reshape(x3,N,N)); daspect([1 1 1]);
%plot(x3,'r.'); axis([0 300 0 ceil(max(x3))]);

%Compute the data for the above configuration compute data based on x1, x2
%and x3

b1 = A*x1;
b2 = A*x2;
b3 = A*x3;
figure; plot(b1); hold on; plot(b2); plot(b3);

