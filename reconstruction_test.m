%--------------------------------------------------------------------------
%Organisation: School of Engineering, University of Edinburgh
%Project: BEng Project
%Name: Xu Zuo s1678439
%Description: This is a demo for image reconstrcution using both Discrete
%Algebraic Reconstruction Technique(DART) and Simultaneous Algebraic
%Reconstruction Technique(SART).
%--------------------------------------------------------------------------

%generate images
N = 100; angle = 0:1:179; p = 100;
theta = sort(angle(randperm(180,10))); %select 10 angles randomly from the interval [0,pi]
[A,b1,x1] = paralleltomo1(N,theta,p);
[~,b2,x2] = paralleltomo2(N,theta,p);
[~,b3,x3] = paralleltomo3(N,theta,p);

%Add noise to data
b1n = poissrnd(b1);
b2n = poissrnd(b2);
b3n = poissrnd(b3);

%produce initial reconstruction
options0.nonneg = true;
x1_0 = sart(A,b1,50,zeros(size(x1)),options0);
x2_0 = sart(A,b2,50,zeros(size(x2)),options0);
x3_0 = sart(A,b3,50,zeros(size(x3)),options0);

%set parameter
R = [0,1,2,3,4]; %define prior information
fix_p = 0.6; %fix probability
s = 0.04; %smoothing parameter
K = 3; %SART iteration within DART loop
iters = 300; %DART iteration

%--------------------------------------------------------------------------
%perform DART
x1_dart = zeros(size(x1,1),iters);
x2_dart = zeros(size(x2,1),iters);
x3_dart = zeros(size(x3,1),iters);

%define pixel errors
pixel_dart1 = zeros(1,iters);
pixel_dart2 = zeros(1,iters);
pixel_dart3 = zeros(1,iters);

%define data errors
data_dart1 = zeros(1,iters);
data_dart2 = zeros(1,iters);
data_dart3 = zeros(1,iters);

%vary the number of iterations and calculate pixel errors
for i = 1:iters
x1_dart(:,i) = iterate_tomo(A,b1,i,x1_0,N,K,R,fix_p,s);
x2_dart(:,i) = iterate_tomo(A,b2,i,x2_0,N,K,R,fix_p,s);
x3_dart(:,i) = iterate_tomo(A,b3,i,x3_0,N,K,R,fix_p,s);

pixel_dart1(:,i) = norm(x1-x1_dart(:,i))/norm(x1);
pixel_dart2(:,i) = norm(x2-x2_dart(:,i))/norm(x2);
pixel_dart3(:,i) = norm(x3-x3_dart(:,i))/norm(x3);

data_dart1(:,i) = norm(b1-A*x1_dart(:,i))/norm(b1);
data_dart2(:,i) = norm(b2-A*x2_dart(:,i))/norm(b2);
data_dart3(:,i) = norm(b3-A*x3_dart(:,i))/norm(b3);
end

%--------------------------------------------------------------------------
%perform SART
x1_sart = zeros(size(x1,1),iters);
x2_sart = zeros(size(x2,1),iters);
x3_sart = zeros(size(x3,1),iters);

pixel_sart1 = zeros(1,iters);
pixel_sart2 = zeros(1,iters);
pixel_sart3 = zeros(1,iters);

data_sart1 = zeros(1,iters);
data_sart2 = zeros(1,iters);
data_sart3 = zeros(1,iters);

options.nonneg = true;
for i = 1:iters
    x1_sart(:,i) = sart(A,b1,i,x1_0,options);
    x2_sart(:,i) = sart(A,b2,i,x2_0,options);
    x3_sart(:,i) = sart(A,b3,i,x3_0,options);
    
    pixel_sart1(:,i) = norm(x1-x1_sart(:,i))/norm(x1);
    pixel_sart2(:,i) = norm(x2-x2_sart(:,i))/norm(x2);
    pixel_sart3(:,i) = norm(x3-x3_sart(:,i))/norm(x3);
    
    data_sart1(:,i) = norm(b1-A*x1_sart(:,i))/norm(b1);
    data_sart2(:,i) = norm(b2-A*x2_sart(:,i))/norm(b2);
    data_sart3(:,i) = norm(b3-A*x3_sart(:,i))/norm(b3);
end

%--------------------------------------------------------------------------
%plotting
figure; 
subplot(2,2,1); imagesc(reshape(x1,N,N)); colorbar horr; daspect([1 1 1]);
subplot(2,2,2); imagesc(reshape(x2,N,N)); colorbar horr; daspect([1 1 1]);
subplot(2,2,3); imagesc(reshape(x3,N,N)); colorbar horr; daspect([1 1 1]);

figure; 
subplot(2,2,1); imagesc(reshape(x1_sart(:,iters),N,N)); colorbar horr; daspect([1 1 1]);
subplot(2,2,2); imagesc(reshape(x2_sart(:,iters),N,N)); colorbar horr; daspect([1 1 1]); 
subplot(2,2,3); imagesc(reshape(x3_sart(:,iters),N,N)); colorbar horr; daspect([1 1 1]);

figure; 
subplot(2,2,1); imagesc(reshape(x1_dart(:,iters),N,N)); colorbar horr; daspect([1 1 1]);
subplot(2,2,2); imagesc(reshape(x2_dart(:,iters),N,N)); colorbar horr; daspect([1 1 1]); 
subplot(2,2,3); imagesc(reshape(x3_dart(:,iters),N,N)); colorbar horr; daspect([1 1 1]);

%% Diagnostics
%compare pixel errors using both DART and SART
figure;
subplot(2,2,1); plot(pixel_dart1,'-o'); hold on; plot(pixel_sart1,'r-x'); xlabel('Number of Iterations'); ylabel('||x1-x1k||/||x1||'); legend('DART','SART');
subplot(2,2,2); plot(pixel_dart2,'-o'); hold on; plot(pixel_sart2,'r-x'); xlabel('Number of Iterations'); ylabel('||x2-x2k||/||x2||'); legend('DART','SART');
subplot(2,2,3); plot(pixel_dart3,'-o'); hold on; plot(pixel_sart3,'r-x'); xlabel('Number of Iterations'); ylabel('||x3-x3k||/||x3||'); legend('DART','SART');

%compare data errors using both DART and SART
figure;
subplot(2,2,1); plot(data_dart1,'-o'); hold on; plot(data_sart1,'r-x'); xlabel('Number of Iterations'); ylabel('||b1-A*b1k||/||b1||'); legend('DART','SART');
subplot(2,2,2); plot(data_dart2,'-o'); hold on; plot(data_sart2,'r-x'); xlabel('Number of Iterations'); ylabel('||b2-A*b2k||/||b2||'); legend('DART','SART');
subplot(2,2,3); plot(data_dart3,'-o'); hold on; plot(data_sart3,'r-x'); xlabel('Number of Iterations'); ylabel('||b3-A*b3k||/||b3||'); legend('DART','SART');