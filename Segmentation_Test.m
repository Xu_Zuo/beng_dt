%--------------------------------------------------------------------------
%Organisation: School of Engineering, University of Edinburgh
%Project: BEng Project
%Name: Xu Zuo s1678439
%Description: This module is a test bench for the seg function.
%--------------------------------------------------------------------------

%Generate an image,Grid Scale 16*16,
%with p = 32 parallel rays for each angle
%Angle increment 180/15 = 12 different angles
N = 20; theta = 0:15:179; p = 2*N;
%the set of known grey levels
R = [0,1,2,3,4];

%The first case
[A,~,x1] = paralleltomo1(N,theta,p);
%use the sgementation function to sort out grey levels in each pixel
X1 = seg(x1,R);

%The second case
%Add uncertainty to the image
[~,~,x2] = paralleltomo2(N,theta,p);
%The sgementation function is able to assign the right value to pixels with
%uncertainties
X2 = seg(x2,R);

%The third case
[~,~,x3] = paralleltomo3(N,theta,p);
%When encountering an unknown grey levels, the function is able to assign
%the closest possible values to those pixels
X3 = seg(x3,R);

figure;
subplot(1,3,1); plot(x1,'*'); hold on; plot(X1,'b.'); axis([0 300 0 ceil(max(x3))]); 
subplot(1,3,2); plot(x2,'g*'); hold on; plot(X2,'b.'); axis([0 300 0 ceil(max(x3))]); 
subplot(1,3,3); plot(x3,'r*'); hold on; plot(X3,'b.'); axis([0 300 0 ceil(max(x3))]);