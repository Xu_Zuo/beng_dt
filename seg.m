%--------------------------------------------------------------------------
%Organisation: School of Engineering, University of Edinburgh
%Project: BEng Project
%Name: Xu Zuo s1678439
%Description: The current reconstruction is segmented to obtian an image 
%that has only a few grey levels (prior knowledge). We use a global 
%threshold scheme for the segmetation.
%--------------------------------------------------------------------------

function [X_seg] = seg(X0,R)
%Input:  X0    The first approxiamte reconstruction matrix is computed 
%        using the ARM (continuous SART algoerithm in this case)
%        R     The set of known grey levels
%Output: X_seg The sgemented image

%Obtain the number of grey levels
%Create the threshold set and calculate the thresholds
T = zeros(size(R)-[0,1]);
for l = 2:numel(R)
    T(l-1) = (R(l-1)+R(l))/2;
end

%Renew grey values in each pixel using global thresholding
X_seg = ones(size(X0))*R(1);
for n = 2:numel(R)
    X_seg(T(n-1) <= X0) = R(n);
end
end



 