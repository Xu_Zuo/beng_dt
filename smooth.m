%--------------------------------------------------------------------------
%Organisation: School of Engineering, University of Edinburgh
%Project: BEng Project
%Name: Xu Zuo s1678439
%Description: This function is a Guassian smoothing filter of radius 1. 
%Reducing the number of variables by fiing a subset of pixels can cause
%heavy fluctauitons in the values of the pixels that are not fixed. This
%function is applied as a final step of each iteration. 
%--------------------------------------------------------------------------
function x_out = smooth(x_in,b)
%Input:  x_in           Reconstructed image,N by N array
%        b              Intensity of smoothing. Between 0 and 1.
%Output: x_out          Filtered image,N by N array
			
			r = 1;
			w = 2 * r + 1;
			
			% Set Kernel
			K = ones(w) * b / (w.^2-1); % edges
			K(r+1,r+1) = 1 - b; % center
			
			% output window
			x_out = zeros(size(x_in,1) + w-1, size(x_in,2) + w - 1);

			% blur convolution
			for s = -r:r 
				for t = -r:r 
					x_out(1+r+s:end-r+s, 1+r+t:end-r+t) = x_out(1+r+s:end-r+s, 1+r+t:end-r+t) + K(r+1+s, r+1+t) * x_in;
				end
			end
			
			% shrink output window
			x_out = x_out(1+r:end-r, 1+r:end-r);
			
		end