%Imaging setup 
N = 100; theta = 0:10:179; p = 100;

%Make connectivity matrix 
C = make_connectivity_matrix(N);

%Set assumed number of clusters
k=5;

%Compute model, data and true image
[A,b,x] = paralleltomo1(N,theta,p);

%Plot target image
%figure; imagesc(reshape(x,N,N)); axis square; colorbar;
 
x0 = zeros(size(x));
iters = 200;
options.nonneg = true;
x0 = sart(A,b,iters,x0,options);

%Perform k-means to create an initial clustering
idx = kmeans(x0,5);

%Form the respective picewise constant characteristics based on the k-means
%result
Phi = zeros(length(x),k);
for i=1:k
Phi(idx==i,i)=1;
end

%Segment the clustered image above
r = lsqnonneg(A*Phi,b);

%The segmented image is
xd = Phi*r;

%This imposes: cluster Phi(:,i) has pixels with value equal to r(i), for i=1,...,k 

%Evaluate the magnitude of the gradient of the current image estimate,
%assuming uniform grid
[gxd,gyd] = gradient(reshape(xd,N,N));
gmd = sqrt(gxd.^2 + gyd.^2);

%Begin reclustering iterations


lambda = 1e-1;

%Assign a discrete probability for each pixel in the image to belong in one
%of the k-clusters based on its data misfit and pixel correlations to its 
%neighbouring pixels based on the existing clustering. Then normalise to 
%make a valid distribution. Once this distributions is made then we sample 
%the next clustering and adjust the segmentation by solving LS

%Pixel clustering probability
p_xi = zeros(N^2,k);

for i=1:length(x)
    
    %find the current cluster of the i'th pixel
    clust_of_pix_i = find(Phi(i,:));
    
    %it's current value based on the previous segmentation is 
    %x(i)
    
    %gather its neighboring pixels
    neigh_of_i = nonzeros(C(i,:));
    
    %compute data misfit for when the value of the pixel i switches to
    %those assigned to the other clusters
    
    %measurements affected by the value of x(i) are
    Qi = find(A(:,i));
    b_Qi = b(Qi);
    
    %compute new set of sub-data when x(i) switches to different clusters
    mis = zeros(k,1);
    smt = zeros(k,1);
    
    for c=1:k
       %make proposal solution with x(i) element of the image belonging to
       %another cluster, say cluster c for c=1,...,k
       xc = xd; xc(i) = r(c); 
       mis(c) = norm(b_Qi - A(Qi,:)*xc)/norm(b_Qi); 
       softmax_mis = exp(-mis);%/sum(exp(-mis));
       
       %compute the prior based on smoothness and the magnitude of the
       %gradient of the image. For the pixel x(i)
       smt(c) = var(xc([i;neigh_of_i]))/mean(xc([i;neigh_of_i]));
       softmax_smt = lambda*exp(-smt);%/sum(exp(-smt));
       
       
       softm = (softmax_mis.*softmax_smt)./sum(softmax_mis.*softmax_smt);
    end
    
        %make a Bayesian probability for the x(i) element to belong to the j'th
        %cluster for j=1,...,k
        p_xi(i,:) = softm;
end

for i=1:N^2
    idx(i) = datasample(1:k,1,'Replace',true,'Weights',p_xi(i,:));
end

Phin = zeros(length(x),k);
for i=1:k
Phin(idx==i,i)=1;
end

%Segment the clustered image above
rn = lsqnonneg(A*Phin,b);

%The segmented image is
xdn = Phin*rn;

