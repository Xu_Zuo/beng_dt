function C = make_connectivity_matrix(N)

C = zeros(N^2,8);

%four corner pixels - 3 neighbours
C(1,1:3) = [2,N+1,N+2];
C(N,1:3) = [N-1,2*N-1,2*N];
C((N-1)*N+1,1:3) = [(N-2)*N+1,(N-2)*N+2,(N-1)*N+2];
C(N^2,1:3) = [N^2-N-1, N^2-N,N^2-1];

%edge pixels - 5 neighbours
%left image side 
for i=2:1:N-1
    C(i,1:5) = [i-1,i+1,(i-1)+N,i+N,i+N+1];
end
%right image side
for i=(N-1)*N+2:1:N^2-1
    C(i,1:5) = [i-1,i+1,(i-1)-N,i-N,i-N+1];
end
%top image side
for i=N+1:N:(N-2)*N+1
   C(i,1:5) = [i-N,i-N+1,i+1,i+N,i+N+1]; 
end
%bottom image side
for i=2*N:N:(N-1)*N
    C(i,1:5) = [i-N,i-N-1,i-1,i+N-1,i+N];
end

%boundary pixels
Bn = find(C(:,1));

%interior pixels
In = setdiff(1:N^2,Bn)';

for i=1:length(In)
    C(In(i),1:8) = [In(i)-1,In(i)+1,In(i)+N,In(i)-N,In(i)+N+1 ...
        In(i)+N-1,In(i)-N+1,In(i)-N-1];
end