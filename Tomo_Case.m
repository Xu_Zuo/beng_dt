%--------------------------------------------------------------------------
%Organisation: School of Engineering, University of Edinburgh
%Project: BEng Project
%Name: Xu Zuo s1678439
%Description: This code is used to set up 3 kinds of tomographic problems.
%The first case displays noiseless image with 5 grey levels; 
%The second case displays noisy iamge, whose grey levels can be described 
%as normal distributions with variance of 0.01;
%The third case displays noisy image with impurities, which can be
%described as an unknown grey level.
%--------------------------------------------------------------------------

%The first case
%Generate a noisless image,Grid Scale 16*16,
%with(f*N^2)=256 rays in random directions penetrate this domain.
[A,~,x1] = tomo(16,1);
%figure
%imagesc(reshape(x1,16,16))
%figure
%plot(x1,'*')
%hold on

%The second case
%Add noise to the image, diplay pixel values and compare them with thos of
%noiseless image
[~,~,x2] = tomo2(16,1);
%title('Uncertain Image')
%plot(x2,'*')
%figure
%imagesc(reshape(x2,16,16))

%The third case
[~,~,x3] = tomo3(16,1);
%figure
%plot(x1,'*')
%hold on
%title('Uncertain Image with Impurities')
%plot(x3,'*')
%figure
%imagesc(reshape(x3,16,16))

figure; 
subplot(1,3,1); plot(x1,'.'); axis([0 300 0 ceil(max(x3))]); 
subplot(1,3,2); plot(x2,'g.'); axis([0 300 0 ceil(max(x3))]); 
subplot(1,3,3); plot(x3,'r.'); axis([0 300 0 ceil(max(x3))]);

%Compute tomographic data for a model A using the three different images:
%x1, x2, and x3
b1 = A*x1;
b2 = A*x2;
b3 = A*x3;
figure; plot(x1); hold on; plot(x2); plot(x3);

