%--------------------------------------------------------------------------
%Code for histo-tomography, based on:
%Organisation: School of Engineering, University of Edinburgh
%Project: BEng Project
%Name: Xu Zuo s1678439
%Description: This is a demo for binary image reconstrcution using both 
%Discrete Algebraic Reconstruction Technique(DART) and Simultaneous 
%Algebraic Reconstruction Technique(SART).
%--------------------------------------------------------------------------

%generate images
N = 100; theta = 0:1:179; p = 100;
[A,~,~] = paralleltomo1(N,theta,p);

%Make a smooth nonnegative image
xsm = abs(peaks(N)); xsm = xsm(:);

%Compute exact data for the image
b = A*xsm;

B=10;

%For x we expect values within some B bins having these edges
edges = linspace(0, max(xsm)+std(xsm)/2,B);
[nn,edge,bin]=histcounts(xsm,edges);

%The vector imask contains the characteristic functions of Radon transform
%integrals for each bin, i.e. at the ith bin we measure \int x(bin(i)) dw
xlev=zeros(length(xsm),B); 

%The levelset images of image xsum. Some bins could be empty!
for i=1:B
xlev(bin==i,i) = xsm(bin==i);
end

blev=zeros(length(b),B);
%The data of the levelset images
for i=1:B
blev(:,i) = A*xlev(:,i);
end

%Check for consistency, e.g. these should be almost zero
check1 = [norm(xsm-sum(xlev,2)), norm(b-sum(blev,2))];

%We can also "measure" the 2nd and 3rd moments. Compute from bin data and
%then check by taking the radon transform of xsm.^2 and xsm.^3
%first moment of xsm is simply the integral of xsm over the domain. In 
%discrete terms
%mom1 = sum(xsm); 
%mom1stat = mean(xsm)*length(xsm);
%mom2 = sum(xsm.^2); 
%mom2stat = (var(xsm) + mean(xsm)^2)*length(xsm);
%mom3 = sum(xsm.^3); 
%mom3stat = (skewness(x)*std(x)^3 + mean(x)^3 + 3*mean(x)*var(x))*length(x);


%The n'th moment of the histodata is the Radon transform of the n'th
%powered image x^n
%[mean(blev(120,:))*size(blev,2),A(120,:)*xsm]


%Add Poisson noise to data
blevn = zeros(size(blev));
err = zeros(B,1);
for i=1:B
blevn(:,i) = poissrnd(blev(:,i)); 
err(i) = norm(blev(:,i)-blevn(:,i))/norm(blev(:,i));
end


%produce initial reconstructions based on a few SART iterations
options.nonneg=true;
xsart0 = zeros(size(xsm,1),B);
for i=1:B
    xsart0(:,i) = sart(A,blevn(:,i),100,zeros(size(xsm)),options);
end


%set parameters
%set the possible image values based on initial SART reconstructions
R = [0,0,1;0,1,2;0,2,3;0,3,4;0,4,5;0,5,6;0,6,7;0,7,8;0,0,8;0,0,0];
fix_p = 0.6;
s = 0.5;
K = 3;
iters = 200;

%perform dart
xdart = zeros(size(xsm,1),B);
for i=1:B
xdart(:,i) = iterate_tomo(A,blevn(:,i),iters,xsart0(:,i),N,K,R(i,:),fix_p,s);
end

% xsart = zeros(size(xsm,1),B);
% for i=1:B
% xsart(:,i) = iterate_tomo(A,blevn(:,i),iters,xsart0(:,i),N,K,R(i,:),fix_p,s);
% end


%perform iters iterations of SART with nonnegativity constraints
xsart = zeros(size(xsm,1),B);
for i=1:size(blevn,2)
xsart(:,i) = sart(A,blevn(:,i),iters,abs(xsart0(:,i)),'nonneg');
end


%Assemble the SART and DART images
xfsart = sum(xsart,2);
xfdart = sum(xdart,2);

%Perhaps regularise before adding?
%?????

figure; 
subplot(1,3,1); imagesc(reshape(xsm,N,N)); colorbar horr; daspect([1 1 1]);
subplot(1,3,2); imagesc(reshape(xfsart,N,N)); colorbar horr; daspect([1 1 1]);
subplot(1,3,3); imagesc(reshape(xfdart,N,N)); colorbar horr; daspect([1 1 1]);


%Image errors
%Explain by simulatuion why are the SART images more accurate despite that
%they look worse than the DART ones.
[norm(xsm-xfsart)/norm(xsm), norm(xsm-xfdart(:))/norm(xsm)]

%Data errors
%Explain
[norm(b-A*xfsart)/norm(b), norm(b-A*xfdart(:))/norm(b)]
