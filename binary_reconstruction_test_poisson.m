%--------------------------------------------------------------------------
%Organisation: School of Engineering, University of Edinburgh
%Project: BEng Project
%Name: Xu Zuo s1678439
%Description: This is a demo for binary image reconstrcution using both 
%Discrete Algebraic Reconstruction Technique(DART) and Simultaneous 
%Algebraic Reconstruction Technique(SART). In this experiment, the two 
%known image values are 0(background) and 4(feature).
%--------------------------------------------------------------------------

%generate images
N = 100; theta = 0:10:179; p = 100;
[A,b1,x1] = paralleltomo1(N,theta,p);

%For x1 we expect values within some bins (intervals) having these edges
edges_x1 = [0, 0.5, 1.5, 2.5, 3.5, 4.5];
[~,edge1,bin1]=histcounts(x1,edges_x1);

%The levelset images of image x1
x1a = zeros(size(x1)); x1a(bin1==1)=x1(bin1==1);
x1b = zeros(size(x1)); x1b(bin1==2)=x1(bin1==2);
x1c = zeros(size(x1)); x1c(bin1==3)=x1(bin1==3);
x1d = zeros(size(x1)); x1d(bin1==4)=x1(bin1==4);
x1e = zeros(size(x1)); x1e(bin1==5)=x1(bin1==5);

%The data of the levelset images of x1
b1a = A*x1a; b1b = A*x1b; b1c = A*x1c; b1d = A*x1d; b1e = A*x1e;

%Check for consistency, e.g. these should be almost zero
[norm(x1 - (x1a+x1b+x1c+x1d+x1e)), norm(b1 - (b1a+b1b+b1c+b1d+b1e))]

%Generate another image x2
[~,b2,x2] = paralleltomo2(N,theta,p);

%For x2 we expect values within some bins (intervals) having these edges
edges_x2 = [0, 0.5, 1.5, 2.5, 3.5, 4.5];
[~,edge2,bin2] =histcounts(x2,edges_x2);

%The levesets for image x2
x2a = zeros(size(x2)); x2a(bin2==1)=x2(bin2==1);
x2b = zeros(size(x2)); x2b(bin2==2)=x2(bin2==2);
x2c = zeros(size(x2)); x2c(bin2==3)=x2(bin2==3);
x2d = zeros(size(x2)); x2d(bin2==4)=x2(bin2==4);
x2e = zeros(size(x2)); x2e(bin2==5)=x2(bin2==5);

%The data of the levelset images of x2
b2a = A*x2a; b2b = A*x2b; b2c = A*x2c; b2d = A*x2d; b2e = A*x2e;

%Check for consistency, e.g. these should be almost zero
[norm(x2 - (x2a+x2b+x2c+x2d+x2e)), norm(b2 - (b2a+b2b+b2c+b2d+b2e))]


%Add noise to data
b1an = poissrnd(b1a); b1bn = poissrnd(b1b); b1cn = poissrnd(b1c); b1dn = poissrnd(b1d);
b1en = poissrnd(b1e); 

b1n =[b1an,b1bn,b1cn,b1dn,b1en];

b2an = poissrnd(b2a); b2bn = poissrnd(b2b); b2cn = poissrnd(b2c); b2dn = poissrnd(b2d);
b2en = poissrnd(b2e); 

b2n =[b2an,b2bn,b2cn,b2dn,b2en];

%produce initial reconstructions based on a few SART iterations
options.nonneg = true;
x1_0 = zeros(size(x1,1),size(b1n,2));
for i=1:size(b1n,2)
x1_0(:,i) = sart(A,b1n(:,i),50,zeros(size(x1)),options);
end
%x1_0 = reshape(x1_0,N,N);

x2_0 = zeros(size(x2,1),size(b2n,2));
for i=1:size(b2n,2)
x2_0(:,i) = sart(A,b2n(:,i),50,zeros(size(x2)),options);
end
%x2_0 = reshape(x2_0,N,N);

%set parameter
%set the possible image values for each binary part
R = [0,0;0,1;0,2;0,3;0,4];
fix_p = 0.6;
s = 0.5;
K = 3;
iters = 200;

%perform dart

x1_dart = zeros(size(x1,1),size(b1n,2));
for i=1:size(b1n,2)
x1_dart(:,i) = iterate_tomo(A,b1n(:,i),iters,x1_0(:,i),N,K,R(i,:),fix_p,s);
end

x2_dart = zeros(size(x2,1),size(b2n,2));
for i=1:size(b2n,2)
x2_dart(:,i) = iterate_tomo(A,b2n(:,i),iters,x2_0(:,i),N,K,R(i,:),fix_p,s);
end


%perform sart
x1_sart = zeros(size(x1,1),size(b1n,2));
for i=1:size(b1n,2)
x1_sart(:,i) = sart(A,b1n(:,i),iters,x1_0(:,i),options);
end

x2_sart = zeros(size(x2,1),size(b2n,2));
for i=1:size(b2n,2)
x2_sart(:,i) = sart(A,b2n(:,i),iters,x2_0(:,i),options);
end

%Assemble the sart and dart images
x1sart = sum(x1_sart,2);
x2sart = sum(x2_sart,2);

x1dart = sum(x1_dart,2);
x2dart = sum(x2_dart,2);


figure; 
subplot(2,3,1); imagesc(reshape(x1,N,N)); daspect([1 1 1]);
subplot(2,3,4); imagesc(reshape(x2,N,N)); daspect([1 1 1]);
subplot(2,3,2); imagesc(reshape(x1sart,N,N)); daspect([1 1 1]);
subplot(2,3,3); imagesc(reshape(x1dart,N,N)); daspect([1 1 1]);
subplot(2,3,5); imagesc(reshape(x2sart,N,N)); daspect([1 1 1]);
subplot(2,3,6); imagesc(reshape(x2dart,N,N)); daspect([1 1 1]);

figure;
subplot(2,2,1); imagesc(reshape(x1_dart(:,2),N,N)); daspect([1 1 1]);
subplot(2,2,2); imagesc(reshape(x1_dart(:,3),N,N)); daspect([1 1 1]);
subplot(2,2,3); imagesc(reshape(x1_dart(:,4),N,N)); daspect([1 1 1]);
subplot(2,2,4); imagesc(reshape(x1_dart(:,5),N,N)); daspect([1 1 1]);


%Image errors
%Explain by simulatuion why are the SART images more accurate despite that
%they look worse than the DART ones.
[norm(x1-x1sart)/norm(x1), norm(x1-x1dart(:))/norm(x1)]
[norm(x2-x2sart)/norm(x2), norm(x2-x2dart(:))/norm(x2)]

%Data errors
%Explain
[norm(b1-A*x1sart)/norm(b1), norm(b1-A*x1dart(:))/norm(b1)]
[norm(b2-A*x2sart)/norm(b2), norm(b2-A*x2dart(:))/norm(b2)]