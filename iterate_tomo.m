%--------------------------------------------------------------------------
%Organisation: School of Engineering, University of Edinburgh
%Project: BEng Project
%Name: Xu Zuo s1678439
%Description: This is a iterative function designed for Discrete Algebraic 
%Reconstruction Technique(DART).
%Input:   A           Coefficient matrix
%         b           Projection data
%         x_in        Initially approximated image
%         iters       Number of iterations for DART
%         K           Number of iterations for SART within DART
%         R           Known image values
%         fix_p       fix probability
%         s           smoothing parameter
%Output:  x_out       Reconstructed image
%--------------------------------------------------------------------------
function x_out = iterate_tomo(A,b,iters,x_in,N,K,R,fix_p,s)
			
            x_iter = reshape(x_in,N,N);
            options.nonneg = true;
            iteration = 0;
            stop = 0;
            
			while ~stop
				iteration = iteration + 1;				

				% segmentation
				x_seg = seg(x_iter,R);

				% select update and fixed pixels
				x_mask = mask(x_seg,fix_p);
                x_unmask = 1 - x_mask;
				x_fix = reshape(x_seg .* x_unmask,N*N,1);
                x_update = reshape(x_iter .* x_mask,N*N,1);

				% subtract fixed pixels from the mearsured projection data
                % compute residual projection difference
				residual = b - A*x_fix;
				
				% ART update for free pixels
				x_update = sart(A,residual,K,x_update,options);
                x_update = reshape(x_update,size(x_mask));
                
                %blur
                x_update = smooth(x_update,s);
                
                %assemble pixels
                x_update(x_unmask == 1) = 0;
                x_iter = reshape(x_fix,size(x_mask)) + x_update .* x_mask;
                
                % stopping rule
                if iteration == iters
                    stop = 1;
                end
            end
            
            %final segmetation
            x_out = seg(x_iter,R);
            x_out = reshape(x_out,N*N,1);
            
end