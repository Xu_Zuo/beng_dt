%--------------------------------------------------------------------------
%Organisation: School of Engineering, University of Edinburgh
%Project: BEng Project
%Name: Xu Zuo s1678439
%Description: This is a demo to inverstigate how fix probability will
%affect pixel errors as the smoothing parameter varies.
%--------------------------------------------------------------------------

%generate images
N = 100; angle = 0:1:179; p = 100;
theta = sort(angle(randperm(180,10))); %select 10 angles randomly from the interval [0,pi]
[A,b1,x1] = paralleltomo1(N,theta,p);
[~,b2,x2] = paralleltomo2(N,theta,p);
[~,b3,x3] = paralleltomo3(N,theta,p);

%Add noise to data
b1n = poissrnd(b1);
b2n = poissrnd(b2);
b3n = poissrnd(b3);

%produce initial reconstruction
options0.nonneg = true;
x1_0 = sart(A,b1,50,zeros(size(x1)),options0);
x2_0 = sart(A,b2,50,zeros(size(x2)),options0);
x3_0 = sart(A,b3,50,zeros(size(x3)),options0);

%set parameter
R = [0,1,2,3,4]; %define prior information
fix_p = linspace(0,0.99,100); %fix probability
s = linspace(0,0.1,6); %smoothing parameter
K = 3; %SART iteration within DART loop
iters = 100; %DART iteration

%--------------------------------------------------------------------------
%vary fix probability and calcualte calculate pixel errors
x1_dart = zeros(size(x1,1),6,100);
x2_dart = zeros(size(x2,1),6,100);
x3_dart = zeros(size(x3,1),6,100);

dart_errors1 = zeros(1,6,100);
dart_errors2 = zeros(1,6,100);
dart_errors3 = zeros(1,6,100);

for n = 1:6
    for i = 1:100
        x1_dart(:,n,i) = iterate_tomo(A,b1,iters,x1_0,N,K,R,fix_p(:,i),s(:,n));
        x2_dart(:,n,i) = iterate_tomo(A,b2,iters,x2_0,N,K,R,fix_p(:,i),s(:,n));
        x3_dart(:,n,i) = iterate_tomo(A,b3,iters,x3_0,N,K,R,fix_p(:,i),s(:,n));

        dart_errors1(:,n,i) = norm(x1-x1_dart(:,n,i))/norm(x1);
        dart_errors2(:,n,i) = norm(x2-x2_dart(:,n,i))/norm(x2);
        dart_errors3(:,n,i) = norm(x3-x3_dart(:,n,i))/norm(x3);
    end
end

x1_s1 = reshape(dart_errors1(:,1,:),1,100);
x1_s2 = reshape(dart_errors1(:,2,:),1,100);
x1_s3 = reshape(dart_errors1(:,3,:),1,100);
x1_s4 = reshape(dart_errors1(:,4,:),1,100);
x1_s5 = reshape(dart_errors1(:,5,:),1,100);
x1_s6 = reshape(dart_errors1(:,6,:),1,100);

x2_s1 = reshape(dart_errors2(:,1,:),1,100);
x2_s2 = reshape(dart_errors2(:,2,:),1,100);
x2_s3 = reshape(dart_errors2(:,3,:),1,100);
x2_s4 = reshape(dart_errors2(:,4,:),1,100);
x2_s5 = reshape(dart_errors2(:,5,:),1,100);
x2_s6 = reshape(dart_errors2(:,6,:),1,100);

x3_s1 = reshape(dart_errors3(:,1,:),1,100);
x3_s2 = reshape(dart_errors3(:,2,:),1,100);
x3_s3 = reshape(dart_errors3(:,3,:),1,100);
x3_s4 = reshape(dart_errors3(:,4,:),1,100);
x3_s5 = reshape(dart_errors3(:,5,:),1,100);
x3_s6 = reshape(dart_errors3(:,6,:),1,100);

figure;
subplot(2,2,1); plot(fix_p,x1_s1,'-x'); hold on; plot(fix_p,x1_s2,'k-x'); hold on;
plot(fix_p,x1_s3,'m-x'); hold on; plot(fix_p,x1_s4,'c-x'); hold on;
plot(fix_p,x1_s5,'r-x'); hold on; plot(fix_p,x1_s6,'g-x');
xlabel('Fix probability'); ylabel('||x1-x1k||/||x1||');
legend('s=0','s=0.02','s=0.04','s=0.06','s=0.08','s=0.1');

subplot(2,2,2); plot(fix_p,x2_s1,'-x'); hold on; plot(fix_p,x2_s2,'k-x'); hold on;
plot(fix_p,x2_s3,'m-x'); hold on; plot(fix_p,x2_s4,'c-x'); hold on;
plot(fix_p,x2_s5,'r-x'); hold on; plot(fix_p,x2_s6,'g-x');
xlabel('Fix probability'); ylabel('||x2-x2k||/||x2||');
legend('s=0','s=0.02','s=0.04','s=0.06','s=0.08','s=0.1');

subplot(2,2,3); plot(fix_p,x3_s1,'-x'); hold on; plot(fix_p,x3_s2,'k-x'); hold on;
plot(fix_p,x3_s3,'m-x'); hold on; plot(fix_p,x3_s4,'c-x'); hold on;
plot(fix_p,x3_s5,'r-x'); hold on; plot(fix_p,x3_s6,'g-x');
xlabel('Fix probability'); ylabel('||x3-x3k||/||x3||');
legend('s=0','s=0.02','s=0.04','s=0.06','s=0.08','s=0.1');