%--------------------------------------------------------------------------
%Organisation: School of Engineering, University of Edinburgh
%Project: BEng Project
%Name: Xu Zuo s1678439
%Description: This is a demo to inverstigate how smoothing parameter will
%affect pixel errors as the fix probability varies.
%--------------------------------------------------------------------------

%generate images
N = 100; angle = 0:1:179; p = 100;
theta = sort(angle(randperm(180,10))); %select 10 angles randomly from the interval [0,pi]
[A,b1,x1] = paralleltomo1(N,theta,p);
[~,b2,x2] = paralleltomo2(N,theta,p);
[~,b3,x3] = paralleltomo3(N,theta,p);

%Add noise to data
b1n = poissrnd(b1);
b2n = poissrnd(b2);
b3n = poissrnd(b3);

%produce initial reconstruction
options0.nonneg = true;
x1_0 = sart(A,b1,50,zeros(size(x1)),options0);
x2_0 = sart(A,b2,50,zeros(size(x2)),options0);
x3_0 = sart(A,b3,50,zeros(size(x3)),options0);

%set parameter
R = [0,1,2,3,4]; %define prior information
fix_p = linspace(0,1,6); %fix probability
s = linspace(0,0.29,30); %smoothing parameter
K = 3; %SART iteration within DART loop
iters = 100; %DART iteration

%--------------------------------------------------------------------------
%vary smoothing parameter and calcualte calculate pixel errors
%K = 3, iters = 100, fix probability = 0.6
x1_dart = zeros(size(x1,1),30,6);
x2_dart = zeros(size(x2,1),30,6);
x3_dart = zeros(size(x3,1),30,6);

dart_errors1 = zeros(1,30,6);
dart_errors2 = zeros(1,30,6);
dart_errors3 = zeros(1,30,6);

for n = 1:30
    for i = 1:6
        x1_dart(:,n,i) = iterate_tomo(A,b1,iters,x1_0,N,K,R,fix_p(:,i),s(:,n));
        x2_dart(:,n,i) = iterate_tomo(A,b2,iters,x2_0,N,K,R,fix_p(:,i),s(:,n));
        x3_dart(:,n,i) = iterate_tomo(A,b3,iters,x3_0,N,K,R,fix_p(:,i),s(:,n));

        dart_errors1(:,n,i) = norm(x1-x1_dart(:,n,i))/norm(x1);
        dart_errors2(:,n,i) = norm(x2-x2_dart(:,n,i))/norm(x2);
        dart_errors3(:,n,i) = norm(x3-x3_dart(:,n,i))/norm(x3);
    end
end

x1_f1 = reshape(dart_errors1(:,:,1),1,30);
x1_f2 = reshape(dart_errors1(:,:,2),1,30);
x1_f3 = reshape(dart_errors1(:,:,3),1,30);
x1_f4 = reshape(dart_errors1(:,:,4),1,30);
x1_f5 = reshape(dart_errors1(:,:,5),1,30);
x1_f6 = reshape(dart_errors1(:,:,6),1,30);

x2_f1 = reshape(dart_errors2(:,:,1),1,30);
x2_f2 = reshape(dart_errors2(:,:,2),1,30);
x2_f3 = reshape(dart_errors2(:,:,3),1,30);
x2_f4 = reshape(dart_errors2(:,:,4),1,30);
x2_f5 = reshape(dart_errors2(:,:,5),1,30);
x2_f6 = reshape(dart_errors2(:,:,6),1,30);

x3_f1 = reshape(dart_errors3(:,:,1),1,30);
x3_f2 = reshape(dart_errors3(:,:,2),1,30);
x3_f3 = reshape(dart_errors3(:,:,3),1,30);
x3_f4 = reshape(dart_errors3(:,:,4),1,30);
x3_f5 = reshape(dart_errors3(:,:,5),1,30);
x3_f6 = reshape(dart_errors3(:,:,6),1,30);

figure;
subplot(2,2,1); plot(s,x1_f1,'-x'); hold on; plot(s,x1_f2,'k-x'); hold on;
plot(s,x1_f3,'m-x'); hold on; plot(s,x1_f4,'c-x'); hold on;
plot(s,x1_f5,'r-x'); hold on; plot(s,x1_f6,'g-x');
xlabel('Smoothing Parameter'); ylabel('||x1-x1k||/||x1||');
legend('fix_p=0','fix_p=0.2','fix_p=0.4','fix_p=0.6','fix_p=0.8','fix_p=1.0');

subplot(2,2,2); plot(s,x2_f1,'-x'); hold on; plot(s,x2_f2,'k-x'); hold on;
plot(s,x2_f3,'m-x'); hold on; plot(s,x2_f4,'c-x'); hold on;
plot(s,x2_f5,'r-x'); hold on; plot(s,x2_f6,'g-x');
xlabel('Smoothing Parameter'); ylabel('||x2-x2k||/||x2||');
legend('fix_p=0','fix_p=0.2','fix_p=0.4','fix_p=0.6','fix_p=0.8','fix_p=1.0');

subplot(2,2,3); plot(s,x3_f1,'-x'); hold on; plot(s,x3_f2,'k-x'); hold on;
plot(s,x3_f3,'m-x'); hold on; plot(s,x3_f4,'c-x'); hold on;
plot(s,x3_f5,'r-x'); hold on; plot(s,x3_f6,'g-x');
xlabel('Smoothing Parameter'); ylabel('||x3-x3k||/||x3||');
legend('fix_p=0','fix_p=0.2','fix_p=0.4','fix_p=0.6','fix_p=0.8','fix_p=1.0');