%--------------------------------------------------------------------------
%Organisation: School of Engineering, University of Edinburgh
%Project: BEng Project
%Name: Xu Zuo s1678439
%Description: This is the initial error analysis of DART, including
%comaprisions of image errors between DART and SART in terms of iterations,
%the effect of fix probability on image errors and the effect of smoothing
%parameter on image errors.
%--------------------------------------------------------------------------

%Generate images
%Grid scale 100x100, number of angles 180, 100 beams per angle
N = 100; theta = 0:1:179; p = 100;
[A,b1,x1] = paralleltomo1(N,theta,p);
[~,b2,x2] = paralleltomo2(N,theta,p);
[~,b3,x3] = paralleltomo3(N,theta,p);

%Add noise to data
b1n = poissrnd(b1);
b2n = poissrnd(b2);
b3n = poissrnd(b3);

%produce initial reconstruction
options0.nonneg = true;
x1_0 = sart(A,b1,50,zeros(size(x1)),options0);
x2_0 = sart(A,b2,50,zeros(size(x2)),options0);
x3_0 = sart(A,b3,50,zeros(size(x3)),options0);

%set parameter
R = [0,1,2,3,4]; %define prior information
fix_p = linspace(0,0.8,5); %fix probability
s = linspace(0,0.1,11); %smoothing parameter
K = 3; %SART iteration within DART loop
iters = 100; %DART iteration

%perform DART
x1_dart = zeros(size(x1,1),iters);
x2_dart = zeros(size(x2,1),iters);
x3_dart = zeros(size(x3,1),iters);

%define pixel errors
dart_errors1 = zeros(1,iters);
dart_errors2 = zeros(1,iters);
dart_errors3 = zeros(1,iters);

%define data errors
dart_data1 = zeros(1,iters);
dart_data2 = zeros(1,iters);
dart_data3 = zeros(1,iters);

%vary the number of iterations and calculate pixel errors
%fix probability = 0.6, smoothing paramter = 0
for i = 1:iters
x1_dart(:,i) = iterate_tomo(A,b1,i,x1_0,N,K,R,fix_p(:,4),s(:,1));
x2_dart(:,i) = iterate_tomo(A,b2,i,x2_0,N,K,R,fix_p(:,4),s(:,1));
x3_dart(:,i) = iterate_tomo(A,b3,i,x3_0,N,K,R,fix_p(:,4),s(:,1));

dart_errors1(:,i) = norm(x1-x1_dart(:,i))/norm(x1);
dart_errors2(:,i) = norm(x2-x2_dart(:,i))/norm(x2);
dart_errors3(:,i) = norm(x3-x3_dart(:,i))/norm(x3);

dart_data1(:,i) = norm(b1-A*x1_dart(:,i))/norm(b1);
dart_data2(:,i) = norm(b2-A*x2_dart(:,i))/norm(b2);
dart_data3(:,i) = norm(b3-A*x3_dart(:,i))/norm(b3);
end

%--------------------------------------------------------------------------
%vary fix probability and calcualte calculate pixel errors
%K = 3, iters = 100, smoothing paramter = 0
x1_fix = zeros(size(x1,1),5);
x2_fix = zeros(size(x2,1),5);
x3_fix = zeros(size(x3,1),5);

fix_errors1 = zeros(1,5);
fix_errors2 = zeros(1,5);
fix_errors3 = zeros(1,5);

for i = 1:5
x1_fix(:,i) = iterate_tomo(A,b1,iters,x1_0,N,K,R,fix_p(:,i),s(:,1));
x2_fix(:,i) = iterate_tomo(A,b2,iters,x2_0,N,K,R,fix_p(:,i),s(:,1));
x3_fix(:,i) = iterate_tomo(A,b3,iters,x3_0,N,K,R,fix_p(:,i),s(:,1));

fix_errors1(:,i) = norm(x1-x1_fix(:,i))/norm(x1);
fix_errors2(:,i) = norm(x2-x2_fix(:,i))/norm(x2);
fix_errors3(:,i) = norm(x3-x3_fix(:,i))/norm(x3);
end

%--------------------------------------------------------------------------
%vary smoothing parameter and calcualte calculate pixel errors
%K = 3, iters = 100, fix probability = 0.6
x1_smooth = zeros(size(x1,1),11);
x2_smooth = zeros(size(x2,1),11);
x3_smooth = zeros(size(x3,1),11);

smooth_errors1 = zeros(1,11);
smooth_errors2 = zeros(1,11);
smooth_errors3 = zeros(1,11);

for i = 1:11
x1_smooth(:,i) = iterate_tomo(A,b1,iters,x1_0,N,K,R,fix_p(:,4),s(:,i));
x2_smooth(:,i) = iterate_tomo(A,b2,iters,x2_0,N,K,R,fix_p(:,4),s(:,i));
x3_smooth(:,i) = iterate_tomo(A,b3,iters,x3_0,N,K,R,fix_p(:,4),s(:,i));

smooth_errors1(:,i) = norm(x1-x1_smooth(:,i))/norm(x1);
smooth_errors2(:,i) = norm(x2-x2_smooth(:,i))/norm(x2);
smooth_errors3(:,i) = norm(x3-x3_smooth(:,i))/norm(x3);
end

%--------------------------------------------------------------------------
%perform SART
x1_sart = zeros(size(x1,1),iters);
x2_sart = zeros(size(x2,1),iters);
x3_sart = zeros(size(x3,1),iters);

sart_errors1 = zeros(1,iters);
sart_errors2 = zeros(1,iters);
sart_errors3 = zeros(1,iters);

sart_data1 = zeros(1,iters);
sart_data2 = zeros(1,iters);
sart_data3 = zeros(1,iters);

%limit recontructed pixel values to [0,4]
options.nonneg = true;
options.box = 4;
for i = 1:iters
    x1_sart(:,i) = sart(A,b1,i,x1_0,options);
    x2_sart(:,i) = sart(A,b2,i,x2_0,options);
    x3_sart(:,i) = sart(A,b3,i,x3_0,options);
    
    sart_errors1(:,i) = norm(x1-x1_sart(:,i))/norm(x1);
    sart_errors2(:,i) = norm(x2-x2_sart(:,i))/norm(x2);
    sart_errors3(:,i) = norm(x3-x3_sart(:,i))/norm(x3);
    
    sart_data1(:,i) = norm(b1-A*x1_sart(:,i))/norm(b1);
    sart_data2(:,i) = norm(b2-A*x2_sart(:,i))/norm(b2);
    sart_data3(:,i) = norm(b3-A*x3_sart(:,i))/norm(b3);
end

%--------------------------------------------------------------------------
%plotting
figure; 
subplot(2,2,1); imagesc(reshape(x1,N,N)); colorbar horr; daspect([1 1 1]);
subplot(2,2,2); imagesc(reshape(x2,N,N)); colorbar horr; daspect([1 1 1]);
subplot(2,2,3); imagesc(reshape(x3,N,N)); colorbar horr; daspect([1 1 1]);

figure; 
subplot(2,2,1); imagesc(reshape(x1_sart(:,100),N,N)); colorbar horr; daspect([1 1 1]);
subplot(2,2,2); imagesc(reshape(x2_sart(:,100),N,N)); colorbar horr; daspect([1 1 1]); 
subplot(2,2,3); imagesc(reshape(x3_sart(:,100),N,N)); colorbar horr; daspect([1 1 1]);

figure; 
subplot(2,2,1); imagesc(reshape(x1_dart(:,100),N,N)); colorbar horr; daspect([1 1 1]);
subplot(2,2,2); imagesc(reshape(x2_dart(:,100),N,N)); colorbar horr; daspect([1 1 1]); 
subplot(2,2,3); imagesc(reshape(x3_dart(:,100),N,N)); colorbar horr; daspect([1 1 1]);

%% Diagnostics
%compare pixel errors using both DART and SART
figure;
subplot(2,2,1); plot(dart_errors1,'-o'); hold on; plot(sart_errors1,'r-x'); xlabel('Number of Iterations'); ylabel('||x1-x1k||/||x1||'); legend('DART','SART');
subplot(2,2,2); plot(dart_errors2,'-o'); hold on; plot(sart_errors2,'r-x'); xlabel('Number of Iterations'); ylabel('||x2-x2k||/||x2||'); legend('DART','SART');
subplot(2,2,3); plot(dart_errors3,'-o'); hold on; plot(sart_errors3,'r-x'); xlabel('Number of Iterations'); ylabel('||x3-x3k||/||x3||'); legend('DART','SART');

%compare data errors using both DART and SART
figure;
subplot(2,2,1); plot(dart_data1,'-o'); hold on; plot(sart_data1,'r-x'); xlabel('Number of Iterations'); ylabel('||b1-A*x1k||/||b1||'); legend('DART','SART','Location','northoutside');
subplot(2,2,2); plot(dart_data2,'-o'); hold on; plot(sart_data2,'r-x'); xlabel('Number of Iterations'); ylabel('||b2-A*x2k||/||b2||'); legend('DART','SART','Location','northoutside');
subplot(2,2,3); plot(dart_data3,'-o'); hold on; plot(sart_data3,'r-x'); xlabel('Number of Iterations'); ylabel('||b3-A*x3k||/||b3||'); legend('DART','SART','Location','northoutside');

%comapre pixel errors using different fix probability
figure;
subplot(2,2,1); plot(fix_p,fix_errors1,'-o'); xlabel('Fix probability'); ylabel('||x1-x1k||/||x1||');
subplot(2,2,2); plot(fix_p,fix_errors2,'-o'); xlabel('Fix probability'); ylabel('||x2-x2k||/||x2||');
subplot(2,2,3); plot(fix_p,fix_errors3,'-o'); xlabel('Fix probability'); ylabel('||x3-x3k||/||x3||');

%compare pixel errors using different smoothing parameter
figure;
subplot(2,2,1); plot(s,smooth_errors1,'-o'); xlabel('Smoothing Parameter'); ylabel('||x1-x1k||/||x1||');
subplot(2,2,2); plot(s,smooth_errors2,'-o'); xlabel('Smoothing Parameter'); ylabel('||x2-x2k||/||x2||');
subplot(2,2,3); plot(s,smooth_errors3,'-o'); xlabel('Smoothing Parameter'); ylabel('||x3-x3k||/||x3||');